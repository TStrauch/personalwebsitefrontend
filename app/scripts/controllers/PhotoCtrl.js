'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:PhotoCtrl
 * @description
 * # PhotoCtrl
 * Controller of the frontendApp
 */
angular.module('site')
  .controller('PhotoCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    this.photos = [
    	{
    		src: "/images/masonry/PhotoWebsite-1.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-2-1.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-2.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-3.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-4.jpg",
    		classes: "hidden-xs"
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-5.jpg",
    		classes: "hidden-xs"
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-6-1.jpg",
    		classes: "hidden-xs"
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-6.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-7.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-8.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-9.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-10.jpg",
    		classes: "hidden-xs"
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-11.jpg",
    		classes: "hidden-xs"
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-12.jpg"
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-13.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-14.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-15.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-16.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-17.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-18.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/PhotoWebsite-19.jpg",
    		classes: ""
    	},
    	{
    		src: "/images/masonry/Underwater01.jpg",
    		classes: ""
    	},
        {
            src: "/images/masonry/Underwater02.jpg",
            classes: ""
        },
    ];
  });
