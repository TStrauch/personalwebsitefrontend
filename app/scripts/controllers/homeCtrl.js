'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the frontendApp
 */
angular.module('site')
  .controller('HomeCtrl', function ($scope, $window, $document, parallaxHelper) {
  	$scope.constants = {};
  	$scope.constants.min_heightImageWrapper = 650;
  	$scope.constants.min_widthImageClass = 1280;
  	$scope.constants.dividerImagesAspectRatio = 0.5078125;

  	$scope.heightImageWrapper = {'height': '650px'};
  	$scope.widthMarginImageClass = {'width': '1280px', 'margin-left': '-640px'};

    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.setWidthMarginImageClass = function(width){
    	if (width < $scope.constants.min_widthImageClass){
    		$scope.widthMarginImageClass = {
    			'width': $scope.constants.min_widthImageClass+'px', 
    			'margin-left': (-1 * $scope.constants.min_widthImageClass / 2)+'px'
    		};
    		$scope.heightImageWrapper = {'height': '650px'};
    	}
    	else{
    		$scope.widthMarginImageClass = {
    			'width': width+'px', 
    			'margin-left': (-1 * width / 2)+'px'
    		};
    		$scope.heightImageWrapper = {'height': (width * $scope.constants.dividerImagesAspectRatio)+'px'};

    	}
    };


    $scope.parallaxBackground = parallaxHelper.createAnimator(-0.3, 150, -150);


    // angular.element(document).ready(function () {
    //     $scope.setHeightImageWrapper($window.innerHeight);
    //     $scope.setWidthMarginImageClass($window.innerWidth);

    // });
  });
