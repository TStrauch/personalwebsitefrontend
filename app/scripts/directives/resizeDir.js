'use strict';
/*jshint unused: false*/
/*jshint -W117 */

angular.module('site')
  .directive('resize', function ($window) {
  	return function(scope, element){

  		var w = angular.element($window);

  		scope.getWindowDimensions = function () {
            return { 'h': $window.innerHeight, 'w': $window.innerWidth };
        };

        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
            scope.setWidthMarginImageClass(newValue.w);

        }, true);

        w.bind('resize', function(){
        	scope.$apply();
        });

  	};
 });